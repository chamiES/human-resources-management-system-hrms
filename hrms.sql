-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2017 at 03:42 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrms`
--

-- --------------------------------------------------------

--
-- Table structure for table `earning_info`
--

CREATE TABLE `earning_info` (
  `EmpCode` varchar(10) NOT NULL DEFAULT '0',
  `Basic` float NOT NULL DEFAULT '0',
  `HouseRent` float NOT NULL DEFAULT '0',
  `Conveyance` float NOT NULL DEFAULT '0',
  `CLA` float NOT NULL DEFAULT '0',
  `Medical` float NOT NULL DEFAULT '0',
  `Special` float NOT NULL DEFAULT '0',
  `OverTime` float NOT NULL DEFAULT '0',
  `Bonous` float NOT NULL DEFAULT '0',
  `PFCounter` float NOT NULL DEFAULT '0',
  `OtherDED2` float NOT NULL DEFAULT '0',
  `EOBIDED` float NOT NULL DEFAULT '0',
  `Other` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `earning_info`
--

INSERT INTO `earning_info` (`EmpCode`, `Basic`, `HouseRent`, `Conveyance`, `CLA`, `Medical`, `Special`, `OverTime`, `Bonous`, `PFCounter`, `OtherDED2`, `EOBIDED`, `Other`) VALUES
('EMP001', 20000, 6000, 15000, 2000, 0, 0, 450, 1, 5, 1000, 2, 8608.33),
('EMP004', 20000, 5000, 5000, 2000, 5000, 2000, 100, 2, 5, 0, 0, 0),
('EMP002', 20000, 4500, 1000, 500, 0, 0, 450, 1, 5, 0, 0, 0),
('EMP003', 25000, 0, 1000, 0, 2600, 1000, 500, 2, 5, 0, 0.5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_info`
--

CREATE TABLE `job_info` (
  `EmpCode` varchar(10) NOT NULL,
  `ModeOfRecruitment` text NOT NULL,
  `DateOfJoining` date NOT NULL,
  `DateOfConfirming` date NOT NULL,
  `DateOfLastPromotion` date NOT NULL,
  `Branch` varchar(20) NOT NULL,
  `Department` varchar(20) NOT NULL,
  `Designation` varchar(15) NOT NULL,
  `Remark` text NOT NULL,
  `CVUploaded` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_info`
--

INSERT INTO `job_info` (`EmpCode`, `ModeOfRecruitment`, `DateOfJoining`, `DateOfConfirming`, `DateOfLastPromotion`, `Branch`, `Department`, `Designation`, `Remark`, `CVUploaded`) VALUES
('EMP001', 'None', '2017-05-04', '2017-05-11', '2017-05-18', 'Anuradhapura', 'IT', 'Administrator', '', 'Yes'),
('EMP000', '', '2017-05-18', '2017-05-10', '2017-05-04', '', '', 'null', '', 'Yes'),
('EMP002', 'None', '2017-07-11', '2017-07-31', '2017-07-11', 'Polonnaruwa', 'IT', 'Manager', '', 'Yes'),
('EMP003', 'N/A', '2014-07-16', '2016-07-04', '2014-07-04', 'Colombo', 'Electronic', 'Employee', 'None', 'Yes'),
('EMP004', 'None', '2017-07-16', '2018-07-16', '2017-07-16', 'Kuliyapitiya', 'IT ', 'Manager', 'None', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `leave_info`
--

CREATE TABLE `leave_info` (
  `EmpCode` varchar(10) NOT NULL,
  `LeaveId` varchar(10) NOT NULL,
  `LeaveType` varchar(15) NOT NULL,
  `StartDate` date NOT NULL,
  `NoOfDays` int(11) NOT NULL,
  `LeavePeriod` varchar(15) NOT NULL,
  `Description` text NOT NULL,
  `ContactNo` text NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_info`
--

INSERT INTO `leave_info` (`EmpCode`, `LeaveId`, `LeaveType`, `StartDate`, `NoOfDays`, `LeavePeriod`, `Description`, `ContactNo`, `Status`) VALUES
('EMP001', 'null', 'Official', '2017-06-13', 0, 'Full Day', 'Family Visit', 'sdsadadqss', 'Rejected'),
('EMP001', '001', 'Official', '2017-06-19', 2, 'Full Day', 'Personal', 'q	fdsfdew', 'Accepted'),
('EMP003', '002', 'Official', '2017-06-19', 2, 'Full Day', 'Family Trip', '0714512369', 'Accepted'),
('EMP002', '003', 'Official', '2017-05-19', 7, 'Full Day', 'Festivel', '0714523979', 'Accepted'),
('EMP002', '004', 'Official', '2017-04-12', 2, 'Full Day', 'Family Trip', '0714512369', 'Accepted'),
('EMP003', '006', 'Official', '2017-07-10', 5, 'Full Day', 'Tour', '0774550359', 'Rejected'),
('EMP003', '007', 'Sick', '2017-07-04', 3, 'Full Day', 'Sick', '0714550359', 'Pending'),
('EMP003', '008', 'Annual', '2017-07-24', 7, 'Full Day', 'Annual leave', '0712312512', 'Accepted');

-- --------------------------------------------------------

--
-- Table structure for table `loandetails`
--

CREATE TABLE `loandetails` (
  `EmpCode` varchar(10) NOT NULL,
  `LoanID` varchar(10) NOT NULL,
  `LoanType` varchar(50) NOT NULL,
  `LoanAmount` float NOT NULL,
  `PaymentPeriod` varchar(10) NOT NULL,
  `PaymentOption` varchar(10) NOT NULL,
  `MoratoriumOnCapitalRepayment` varchar(10) NOT NULL,
  `PurposeOfLoan` text NOT NULL,
  `EstimateValue` float NOT NULL,
  `PurchasePrice` float NOT NULL,
  `NameOfPresentOwner` text NOT NULL,
  `AddressOfProperty` text NOT NULL,
  `Date` date NOT NULL,
  `Status` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loandetails`
--

INSERT INTO `loandetails` (`EmpCode`, `LoanID`, `LoanType`, `LoanAmount`, `PaymentPeriod`, `PaymentOption`, `MoratoriumOnCapitalRepayment`, `PurposeOfLoan`, `EstimateValue`, `PurchasePrice`, `NameOfPresentOwner`, `AddressOfProperty`, `Date`, `Status`) VALUES
('EMP001', 'LN001', 'Standard Home Loan', 100000, '1 year', 'Monthly', '12 months', 'Purchase a fully built House', 120000, 100000, 'perera', 'colombo', '2017-07-22', 'ACCEPTED'),
('EMP002', 'LN002', 'Standard Home Loan', 454212, '5 years', 'Monthly', '24 months', 'For Renovation', 126987, 1643220, 'kamal', 'Chilaw', '2017-07-22', 'PENDING'),
('EMP003', 'LN003', 'Standard Home Loan', 100.5, '25 years', 'Monthly', '36 months', 'For Credit Facility ', 200, 100.5, 'Nimal', 'Ampara', '2017-07-22', 'PENDING'),
('EMP003', 'LN004', 'Standard Home Loan', 250000, '15 years', 'Monthly', '36 months', 'Purchase a unregistered Vehicle', 300000, 250000, 'Chamara', 'Kegalle', '2017-07-24', 'PENDING'),
('EMP003', 'LN005', 'Standard Car Loan', 1000000, '5 years', 'Monthly', '12 months', 'Purchase a unregistered Vehicle', 3000000, 1000000, 'Carsale', 'Colombo', '2017-07-24', 'PENDING');

-- --------------------------------------------------------

--
-- Table structure for table `logdata`
--

CREATE TABLE `logdata` (
  `NIC` varchar(10) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `Password` text NOT NULL,
  `AccessType` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logdata`
--

INSERT INTO `logdata` (`NIC`, `FirstName`, `LastName`, `UserName`, `Password`, `AccessType`) VALUES
('943180202v', 'Chamidu', 'Chanaka', 'Admin', '1111', 'Employee'),
('111111111v', 'Kanishka', 'Auranga', 'Mgr', '2222', 'Employee'),
('', '', '', 'null', 'null', 'Employee'),
('333333333v', 'Methmal', 'Fonseka', 'Emp', '3333', 'Employee');

-- --------------------------------------------------------

--
-- Table structure for table `massege_info`
--

CREATE TABLE `massege_info` (
  `EmpCode` varchar(10) NOT NULL,
  `MailID` varchar(10) NOT NULL,
  `From` varchar(40) NOT NULL,
  `ToR` varchar(40) NOT NULL,
  `Date` date NOT NULL,
  `Time` time NOT NULL,
  `Message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `massege_info`
--

INSERT INTO `massege_info` (`EmpCode`, `MailID`, `From`, `ToR`, `Date`, `Time`, `Message`) VALUES
('EMP001', 'M001', 'W.C.C. Premarathna', 'W.C.C. Premarathna', '2017-07-20', '10:19:00', 'asdsad'),
('EMP002', 'M002', 'abc', 'E.M.M.P. Ekanayaka', '2017-07-11', '08:05:14', 'csdjfhsdkfdsf'),
('EMP001', 'M003', 'sdsad', 'gfghh', '2017-07-10', '06:14:05', 'dgfdpaospa'),
('EMP003', 'M004', 'W.C.C. Premarathna', 'M. Fonseka', '2017-07-21', '08:59:00', 'hchfdsufysk');

-- --------------------------------------------------------

--
-- Table structure for table `personal_info`
--

CREATE TABLE `personal_info` (
  `EmpCode` varchar(10) NOT NULL,
  `AppCode` varchar(10) NOT NULL,
  `FullName` text NOT NULL,
  `NameInitial` varchar(150) NOT NULL,
  `Designation` varchar(15) NOT NULL,
  `Grade` varchar(10) NOT NULL,
  `TypeOfEmployee` varchar(15) NOT NULL,
  `LastUpdated` text NOT NULL,
  `DateOfBirth` date NOT NULL,
  `Salutation` varchar(10) NOT NULL,
  `Nationality` varchar(15) NOT NULL,
  `NIC` varchar(15) NOT NULL,
  `BloodGroup` varchar(5) NOT NULL,
  `Religion` varchar(15) NOT NULL,
  `MaritualState` varchar(10) NOT NULL,
  `Sex` varchar(5) NOT NULL,
  `HouseFlatNo` varchar(5) NOT NULL,
  `StreetName` text NOT NULL,
  `CityTownVillage` text NOT NULL,
  `PostalCode` varchar(20) NOT NULL,
  `ContactNo` varchar(10) NOT NULL,
  `Email` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personal_info`
--

INSERT INTO `personal_info` (`EmpCode`, `AppCode`, `FullName`, `NameInitial`, `Designation`, `Grade`, `TypeOfEmployee`, `LastUpdated`, `DateOfBirth`, `Salutation`, `Nationality`, `NIC`, `BloodGroup`, `Religion`, `MaritualState`, `Sex`, `HouseFlatNo`, `StreetName`, `CityTownVillage`, `PostalCode`, `ContactNo`, `Email`) VALUES
('EMP001', 'APP001', 'Waduge Chamidu Chanaka Premarathna', 'W.C.C. Premarathna', 'Administrator', 'Grade I ', 'Full time', 'N/A', '1994-11-13', 'Mr', 'null', '943180202v', 'A+', 'Buddhists', 'Married', 'Male', '', 'Horowpothana Road', 'Medawachchiya', '50500', '0717982584', 'c@gmail.com'),
('EMP002', 'APP002', 'Ekanayaka Mudiyanselage Menuka Prasad Ekanayaka', 'E.M.M.P. Ekanayaka', 'Manager', 'Grade I ', 'Full time', '', '1994-04-12', 'Mr', 'null', '111111111v', 'B+', 'Buddhists', 'Unmarried', 'Male', '', 'Girithale Road', 'Polonnaruwa', '50200', '0714559812', 'm@gmail.com'),
('EMP003', 'APP003', 'Methmal Fonseka', 'M. Fonseka', 'Employee', 'Grade I ', 'Part time', '', '1993-03-09', 'Mr', 'null', '333333333v', 'A+', 'Christian', 'Unmarried', 'Male', 'No.45', 'Mattakkuliya', 'Colombo', '50000', '0714512369', 'M@gmail.com'),
('EMP004', 'APP004', 'Lishara Sandaruwan', 'L.sandaruwan', 'Manager', 'Grade II', 'Full time', '', '1998-10-09', 'Mr', 'null', '982211122v', 'A+', 'Buddhists', 'Unmarried', 'Male', '', 'Dandagamuwa ', 'Kuliyapitiya', '50510', '0775623895', 'l@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `workinghours`
--

CREATE TABLE `workinghours` (
  `EmpCode` varchar(10) NOT NULL,
  `Year` varchar(10) NOT NULL,
  `Month` varchar(10) NOT NULL,
  `Hours` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workinghours`
--

INSERT INTO `workinghours` (`EmpCode`, `Year`, `Month`, `Hours`) VALUES
('EMP001', '2017', 'January', 180),
('EMP001', '2017', 'February', 160),
('EMP001', '2017', 'March', 170),
('EMP001', '2017', 'April', 140),
('EMP001', '2017', 'May', 126),
('EMP001', '2017', 'June', 190),
('EMP001', '2017', 'July', 145),
('EMP001', '2017', 'August', 126),
('EMP001', '2017', 'September', 176),
('EMP001', '2017', 'October', 100),
('EMP001', '2017', 'November', 135),
('EMP001', '2017', 'December', 178);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `earning_info`
--
ALTER TABLE `earning_info`
  ADD PRIMARY KEY (`EmpCode`);

--
-- Indexes for table `job_info`
--
ALTER TABLE `job_info`
  ADD PRIMARY KEY (`EmpCode`);

--
-- Indexes for table `leave_info`
--
ALTER TABLE `leave_info`
  ADD PRIMARY KEY (`LeaveId`);

--
-- Indexes for table `loandetails`
--
ALTER TABLE `loandetails`
  ADD PRIMARY KEY (`LoanID`);

--
-- Indexes for table `logdata`
--
ALTER TABLE `logdata`
  ADD PRIMARY KEY (`NIC`);

--
-- Indexes for table `massege_info`
--
ALTER TABLE `massege_info`
  ADD PRIMARY KEY (`MailID`);

--
-- Indexes for table `personal_info`
--
ALTER TABLE `personal_info`
  ADD PRIMARY KEY (`NIC`);

--
-- Indexes for table `workinghours`
--
ALTER TABLE `workinghours`
  ADD PRIMARY KEY (`Month`,`Year`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
