/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hrms;

import static hrms.ProfileMGR.jComboBox1;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Chamidu Chanaka
 */
public class LoanMGR extends javax.swing.JFrame {

    /**
     * Creates new form LoanMGR
     */
    public LoanMGR(String user) {
        initComponents();
        setMyDate();

        String nic = " ";
        String newNic = " ";
        String Ecode = "";
        String AppCode = "";
        String name = "";
        String des = "";
        String grade = "";
        String type = "";

        try {

            Connection c = DBCON.con();
            Statement st = c.createStatement();
            String sql = "SELECT NIC FROM Logdata WHERE UserName ='" + user + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                nic = rs.getString("NIC");

            }

            String sql2 = "SELECT * FROM personal_info WHERE NIC ='" + nic + "'";
            ResultSet rs2 = st.executeQuery(sql2);

            while (rs2.next()) {
                newNic = rs2.getString("NIC");

                Ecode = rs2.getString("EmpCode");
                AppCode = rs2.getString("AppCode");
                name = rs2.getString("NameInitial");
                des = rs2.getString("Designation");
                grade = rs2.getString("Grade");
                type = rs2.getString("TypeOfEmployee");

                NIC.setText(newNic);
                this.EmpCode.setText(Ecode);
                this.AppCode.setText(AppCode);
                NameInitial.setText(name);
                Designation.setText(des);
                Grade.setText(grade);
                TypeOfEmployee.setText(type);
            }

            String sql3 = "SELECT * FROM loandetails";
            ResultSet rs3 = st.executeQuery(sql3);

            int i = 0;
            String EcodeIDT = "";
            String LoanIDT = "";
            String LoanTypeT = "";
            String LoanAmountT = "";
            String PaymentPeriodT = "";
            String PaymentOptionT = "";
            String MoratoriumOnCapitalRepaymentT = "";
            String PurposeOfLoanT = "";
            String EstimateValueT = "";
            String PurchasePriceT = "";
            String NameOfPresentOwnerT = "";
            String AddressOfPropertyT = "";
            String DateT = "";
            String StatusT = "";

            while (rs3.next()) {
                EcodeIDT = rs3.getString("EmpCode");
                LoanIDT = rs3.getString("LoanID");
                LoanTypeT = rs3.getString("LoanType");
                LoanAmountT = rs3.getString("LoanAmount");
                PaymentPeriodT = rs3.getString("PaymentPeriod");
                PaymentOptionT = rs3.getString("PaymentOption");
                MoratoriumOnCapitalRepaymentT = rs3.getString("MoratoriumOnCapitalRepayment");
                PurposeOfLoanT = rs3.getString("PurposeOfLoan");
                EstimateValueT = rs3.getString("EstimateValue");
                PurchasePriceT = rs3.getString("PurchasePrice");
                NameOfPresentOwnerT = rs3.getString("NameOfPresentOwner");
                AddressOfPropertyT = rs3.getString("AddressOfProperty");
                DateT = rs3.getString("Date");
                StatusT = rs3.getString("Status");

                loanTable.setValueAt(EcodeIDT, i, 0);
                loanTable.setValueAt(LoanIDT, i, 1);
                loanTable.setValueAt(LoanTypeT, i, 2);
                loanTable.setValueAt(LoanAmountT, i, 3);
                loanTable.setValueAt(PaymentPeriodT, i, 4);
                loanTable.setValueAt(PaymentOptionT, i, 5);
                loanTable.setValueAt(MoratoriumOnCapitalRepaymentT, i, 6);
                loanTable.setValueAt(PurposeOfLoanT, i, 7);
                loanTable.setValueAt(EstimateValueT, i, 8);
                loanTable.setValueAt(PurchasePriceT, i, 9);
                loanTable.setValueAt(NameOfPresentOwnerT, i, 10);
                loanTable.setValueAt(AddressOfPropertyT, i, 11);
                loanTable.setValueAt(DateT, i, 12);
                loanTable.setValueAt(StatusT, i, 13);

                i++;
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        tableFirstRow();
    }

    public void tableFirstRow() {
        String EcodeIDT = "";
        String LoanIDT = "";
        String LoanTypeT = "";
        String LoanAmountT = "";
        String PaymentPeriodT = "";
        String PaymentOptionT = "";
        String MoratoriumOnCapitalRepaymentT = "";
        String PurposeOfLoanT = "";
        String EstimateValueT = "";
        String PurchasePriceT = "";
        String NameOfPresentOwnerT = "";
        String AddressOfPropertyT = "";
        String DateT = "";
        String StatusT = "";

        EcodeIDT = loanTable.getValueAt(0, 0).toString();
        LoanIDT = loanTable.getValueAt(0, 1).toString();
        LoanTypeT = loanTable.getValueAt(0, 2).toString();
        LoanAmountT = loanTable.getValueAt(0, 3).toString();
        PaymentPeriodT = loanTable.getValueAt(0, 4).toString();
        PaymentOptionT = loanTable.getValueAt(0, 5).toString();
        MoratoriumOnCapitalRepaymentT = loanTable.getValueAt(0, 6).toString();
        PurposeOfLoanT = loanTable.getValueAt(0, 7).toString();
        EstimateValueT = loanTable.getValueAt(0, 8).toString();
        PurchasePriceT = loanTable.getValueAt(0, 9).toString();
        NameOfPresentOwnerT = loanTable.getValueAt(0, 10).toString();
        AddressOfPropertyT = loanTable.getValueAt(0, 11).toString();
        DateT = loanTable.getValueAt(0, 12).toString();
        StatusT = loanTable.getValueAt(0, 13).toString();

        EmpCode1.setText(EcodeIDT);
        LoanID.setText(LoanIDT);
        LoanType.setText(LoanTypeT);
        LoanAmount.setText(LoanAmountT);
        PaymentPeriod.setText(PaymentPeriodT);
        PaymentOption.setText(PaymentOptionT);
        MoratoriumOnCapitalRepayment.setText(MoratoriumOnCapitalRepaymentT);
        PurposeOfLoan.setText(PurposeOfLoanT);
        EstimateValue.setText(EstimateValueT);
        PurchasePrice.setText(PurchasePriceT);
        NameOfPresentOwner.setText(NameOfPresentOwnerT);
        AddressOfProperty.setText(AddressOfPropertyT);
        Date1.setText(DateT);
        this.StatusOfTheLoan.setSelectedItem(StatusT);

    }

    public void setMyDate() {
        Date d = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf1.format(d);
        Date.setText(date);
    }

    public void close() {
        WindowEvent winClosingEvent = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        PurposeOfLoan = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        EmpCode = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        EstimateValue = new javax.swing.JLabel();
        LoanType = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        PaymentPeriod = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        AddressOfProperty = new javax.swing.JLabel();
        NameInitial = new javax.swing.JLabel();
        PaymentOption = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        MoratoriumOnCapitalRepayment = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        Designation = new javax.swing.JLabel();
        AppCode = new javax.swing.JLabel();
        Date1 = new javax.swing.JLabel();
        EmpCode1 = new javax.swing.JTextField();
        LoanAmount = new javax.swing.JLabel();
        PurchasePrice = new javax.swing.JLabel();
        Grade = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        LoanID = new javax.swing.JLabel();
        NameOfPresentOwner = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        loanTable = new javax.swing.JTable();
        TypeOfEmployee = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        NIC = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        StatusOfTheLoan = new javax.swing.JComboBox<>();
        Background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(32, 194, 170));
        jLabel4.setText("Name");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 138, -1));

        PurposeOfLoan.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        PurposeOfLoan.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        PurposeOfLoan.setOpaque(true);
        getContentPane().add(PurposeOfLoan, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 390, 160, 20));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(32, 194, 170));
        jLabel6.setText("Loan ID");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 420, 138, -1));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(32, 194, 170));
        jLabel5.setText("NIC");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 138, -1));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(32, 194, 170));
        jLabel8.setText("Loan Amount                               RS:");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 480, 190, -1));

        jLabel18.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(32, 194, 170));
        jLabel18.setText("Grade");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 110, 138, -1));

        jLabel20.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(32, 194, 170));
        jLabel20.setText("Date");
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 170, 138, -1));

        jLabel14.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(32, 194, 170));
        jLabel14.setText("Name Of present Owner");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 480, 138, -1));

        jLabel12.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(32, 194, 170));
        jLabel12.setText("Status of the Loan");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 570, 130, -1));

        EmpCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        EmpCode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        EmpCode.setOpaque(true);
        getContentPane().add(EmpCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 170, 20));

        jLabel39.setBackground(new java.awt.Color(32, 194, 170));
        jLabel39.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Save");
        jLabel39.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel39.setOpaque(true);
        jLabel39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel39MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel39MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel39MouseExited(evt);
            }
        });
        getContentPane().add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 600, 100, 30));

        EstimateValue.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        EstimateValue.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        EstimateValue.setOpaque(true);
        getContentPane().add(EstimateValue, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 420, 160, 20));

        LoanType.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        LoanType.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        LoanType.setOpaque(true);
        getContentPane().add(LoanType, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 450, 160, 20));

        jLabel21.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(32, 194, 170));
        jLabel21.setText("Purpose Of Loan");
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 390, 138, -1));

        jLabel25.setFont(new java.awt.Font("Century Gothic", 0, 36)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 51, 51));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Loan Details");
        getContentPane().add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 200, 520, -1));

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(32, 194, 170));
        jLabel11.setText("Moratorium on capital repayment");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 570, 180, -1));

        PaymentPeriod.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        PaymentPeriod.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        PaymentPeriod.setOpaque(true);
        getContentPane().add(PaymentPeriod, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 510, 160, 20));

        Date.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Date.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Date.setOpaque(true);
        getContentPane().add(Date, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 170, 170, 20));

        jLabel19.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(32, 194, 170));
        jLabel19.setText("Type of Employee");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 140, 138, -1));

        AddressOfProperty.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        AddressOfProperty.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        AddressOfProperty.setOpaque(true);
        getContentPane().add(AddressOfProperty, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 510, 160, 20));

        NameInitial.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NameInitial.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NameInitial.setOpaque(true);
        getContentPane().add(NameInitial, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, 170, 20));

        PaymentOption.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        PaymentOption.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        PaymentOption.setOpaque(true);
        getContentPane().add(PaymentOption, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 540, 160, 20));

        jLabel17.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(32, 194, 170));
        jLabel17.setText("Designation");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 80, 138, -1));

        MoratoriumOnCapitalRepayment.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        MoratoriumOnCapitalRepayment.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        MoratoriumOnCapitalRepayment.setOpaque(true);
        getContentPane().add(MoratoriumOnCapitalRepayment, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 570, 160, 20));

        jLabel35.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(32, 194, 170));
        jLabel35.setText("Date");
        getContentPane().add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 540, 138, -1));

        Designation.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Designation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Designation.setOpaque(true);
        getContentPane().add(Designation, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 80, 170, 20));

        AppCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        AppCode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        AppCode.setOpaque(true);
        getContentPane().add(AppCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 170, 20));

        Date1.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Date1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Date1.setOpaque(true);
        getContentPane().add(Date1, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 540, 160, 20));

        EmpCode1.setEditable(false);
        EmpCode1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(EmpCode1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 390, 160, 20));

        LoanAmount.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        LoanAmount.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        LoanAmount.setOpaque(true);
        getContentPane().add(LoanAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 480, 160, 20));

        PurchasePrice.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        PurchasePrice.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        PurchasePrice.setOpaque(true);
        getContentPane().add(PurchasePrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 450, 160, 20));

        Grade.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Grade.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Grade.setOpaque(true);
        getContentPane().add(Grade, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 110, 170, 20));

        jLabel16.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(32, 194, 170));
        jLabel16.setText("Estimate Value                              RS:");
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 420, 190, -1));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(32, 194, 170));
        jLabel7.setText("Loan Type");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 450, 138, -1));

        LoanID.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        LoanID.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        LoanID.setOpaque(true);
        getContentPane().add(LoanID, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 420, 160, 20));

        NameOfPresentOwner.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NameOfPresentOwner.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NameOfPresentOwner.setOpaque(true);
        getContentPane().add(NameOfPresentOwner, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 480, 160, 20));

        loanTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        loanTable.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        loanTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Employee Code", "Loan ID", "Loan Type", "Loan Amount", "Payment Period", "payment Option", "M. of capital Repayment", "Purpose of Loan", "Estimate value", "Purchase Value", "Name of Present Ownerl", "Address of Property", "Date", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        loanTable.setGridColor(new java.awt.Color(32, 194, 170));
        loanTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loanTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(loanTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 260, 770, 110));

        TypeOfEmployee.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        TypeOfEmployee.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        TypeOfEmployee.setOpaque(true);
        getContentPane().add(TypeOfEmployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 140, 170, 20));

        jLabel15.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(32, 194, 170));
        jLabel15.setText("Purchase Value                           RS:");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 450, 190, -1));

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(32, 194, 170));
        jLabel2.setText("Employee Code");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 138, -1));

        jLabel10.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(32, 194, 170));
        jLabel10.setText("Payment Option");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 540, 138, -1));

        jLabel13.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(32, 194, 170));
        jLabel13.setText("Address of property");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 510, 138, -1));

        jLabel22.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(32, 194, 170));
        jLabel22.setText("Emp. Code");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 390, 110, 20));

        NIC.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NIC.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NIC.setOpaque(true);
        getContentPane().add(NIC, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 170, 170, 20));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(32, 194, 170));
        jLabel3.setText("Applicant Code");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 138, -1));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(32, 194, 170));
        jLabel9.setText("Payment Period");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 510, 138, -1));

        jLabel40.setBackground(new java.awt.Color(32, 194, 170));
        jLabel40.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("Back");
        jLabel40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel40.setOpaque(true);
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel40MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel40MouseExited(evt);
            }
        });
        getContentPane().add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 600, 100, 30));

        StatusOfTheLoan.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        StatusOfTheLoan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PENDING", "ACCEPTED", "REJECTED" }));
        getContentPane().add(StatusOfTheLoan, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 570, 160, -1));

        Background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hrms/Loan.jpg"))); // NOI18N
        getContentPane().add(Background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 680));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel39MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseExited
        jLabel39.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel39MouseExited

    private void jLabel39MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseClicked
        try {

            Connection c = DBCON.con();
            Statement s = c.createStatement();
            s.executeUpdate("update loandetails set Status ='" + StatusOfTheLoan.getSelectedItem() + "' where LoanID ='" + LoanID.getText() + "'");
           
            String state = StatusOfTheLoan.getSelectedItem().toString();
            //System.out.println(state);

            if (state.equals("ACCEPTED")) {
                
                double nmnth = 0;
                 
                double Amount = Double.parseDouble(LoanAmount.getText());
                String paymentperiod = this.PaymentPeriod.getText();
                
                switch (paymentperiod) {
                    case "1 year":
                        nmnth = 12;
                        break;
                    case "2 years":
                        nmnth = 24;
                        break;
                    case "4 years":
                        nmnth = 48;
                        break;
                    case "5 years":
                        nmnth = 60;
                        break;
                    case "10 years":
                        nmnth = 120;
                        break;
                    case "15 years":
                        nmnth = 180;
                        break;
                    case "25 years":
                        nmnth = 300;
                        break;
                }
                //System.out.println(nmnth);
                
                double installment = (Amount/nmnth)*103.3/100;
                System.out.println("Ammount "+installment);
                
                Statement st = c.createStatement();

                st.executeUpdate("update earning_info set Other  ='" + installment + "' where EmpCode  ='" + EmpCode1.getText() + "'");
            } else{
                
            }
            
             JOptionPane.showMessageDialog(null, "Upadate Successfully");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jLabel39MouseClicked

    private void jLabel39MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseEntered
        jLabel39.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel39MouseEntered

    private void loanTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loanTableMouseClicked
        int row = loanTable.getSelectedRow();

        String EcodeIDT = "";
        String LoanIDT = "";
        String LoanTypeT = "";
        String LoanAmountT = "";
        String PaymentPeriodT = "";
        String PaymentOptionT = "";
        String MoratoriumOnCapitalRepaymentT = "";
        String PurposeOfLoanT = "";
        String EstimateValueT = "";
        String PurchasePriceT = "";
        String NameOfPresentOwnerT = "";
        String AddressOfPropertyT = "";
        String DateT = "";
        String StatusT = "";

        EcodeIDT = loanTable.getValueAt(row, 0).toString();
        LoanIDT = loanTable.getValueAt(row, 1).toString();
        LoanTypeT = loanTable.getValueAt(row, 2).toString();
        LoanAmountT = loanTable.getValueAt(row, 3).toString();
        PaymentPeriodT = loanTable.getValueAt(row, 4).toString();
        PaymentOptionT = loanTable.getValueAt(row, 5).toString();
        MoratoriumOnCapitalRepaymentT = loanTable.getValueAt(row, 6).toString();
        PurposeOfLoanT = loanTable.getValueAt(row, 7).toString();
        EstimateValueT = loanTable.getValueAt(row, 8).toString();
        PurchasePriceT = loanTable.getValueAt(row, 9).toString();
        NameOfPresentOwnerT = loanTable.getValueAt(row, 10).toString();
        AddressOfPropertyT = loanTable.getValueAt(row, 11).toString();
        DateT = loanTable.getValueAt(row, 12).toString();
        StatusT = loanTable.getValueAt(row, 13).toString();

        EmpCode1.setText(EcodeIDT);
        LoanID.setText(LoanIDT);
        LoanType.setText(LoanTypeT);
        LoanAmount.setText(LoanAmountT);
        PaymentPeriod.setText(PaymentPeriodT);
        PaymentOption.setText(PaymentOptionT);
        MoratoriumOnCapitalRepayment.setText(MoratoriumOnCapitalRepaymentT);
        PurposeOfLoan.setText(PurposeOfLoanT);
        EstimateValue.setText(EstimateValueT);
        PurchasePrice.setText(PurchasePriceT);
        NameOfPresentOwner.setText(NameOfPresentOwnerT);
        AddressOfProperty.setText(AddressOfPropertyT);
        Date1.setText(DateT);
        this.StatusOfTheLoan.setSelectedItem(StatusT);
    }//GEN-LAST:event_loanTableMouseClicked

    private void jLabel40MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseExited
       jLabel40.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel40MouseExited

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        new AdminHome2().setVisible(true);
        close();
    }//GEN-LAST:event_jLabel40MouseClicked

    private void jLabel40MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseEntered
       jLabel40.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel40MouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoanMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoanMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoanMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoanMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoanMGR(Login.UserName.getText()).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AddressOfProperty;
    private javax.swing.JLabel AppCode;
    private javax.swing.JLabel Background;
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Date1;
    private javax.swing.JLabel Designation;
    private javax.swing.JLabel EmpCode;
    private javax.swing.JTextField EmpCode1;
    private javax.swing.JLabel EstimateValue;
    private javax.swing.JLabel Grade;
    private javax.swing.JLabel LoanAmount;
    private javax.swing.JLabel LoanID;
    private javax.swing.JLabel LoanType;
    private javax.swing.JLabel MoratoriumOnCapitalRepayment;
    private javax.swing.JLabel NIC;
    private javax.swing.JLabel NameInitial;
    private javax.swing.JLabel NameOfPresentOwner;
    private javax.swing.JLabel PaymentOption;
    private javax.swing.JLabel PaymentPeriod;
    private javax.swing.JLabel PurchasePrice;
    private javax.swing.JLabel PurposeOfLoan;
    private javax.swing.JComboBox<String> StatusOfTheLoan;
    private javax.swing.JLabel TypeOfEmployee;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable loanTable;
    // End of variables declaration//GEN-END:variables
}
