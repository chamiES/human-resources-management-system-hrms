/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hrms;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Chamidu Chanaka
 */
public class PayrollMGR extends javax.swing.JFrame {

    /**
     * Creates new form PayrollMGR
     */
    public PayrollMGR() {
        initComponents();
        fillEmp();
        setMyDate();
    }

    public void fillEmp() {
        String EmpCode = " ";
        try {
            Connection c = DBCON.con();
            Statement st = c.createStatement();
            String sql = "SELECT * FROM personal_info";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                EmpCode = rs.getString("EmpCode");
                this.EmpCode.addItem(EmpCode);

            }
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void close() {
        WindowEvent winClosingEvent = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }
    
    public void setMyDate() {
      Date d=new Date();
      SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
      String date=sdf1.format(d);
      Date.setText(date);  
   }
    
    public void calculteSalary(){
       String date = Date.getText();
       String year = date.substring(0, 4);
       String month = date.substring(5,7);
       String monthName = "";
       
       switch(month){
           case "01":
               monthName = "December";
               break;
            case "02":
               monthName = "January";
               break;
            case "03":
               monthName = "February";
               break;
            case "04":
               monthName = "March";
               break;
            case "05":
               monthName = "April";
               break;
            case "06":
               monthName = "May";
               break;
            case "07":
               monthName = "June";
               break;
            case "08":
               monthName = "July";
               break;
            case "09":
               monthName = "August";
               break;
            case "10":
               monthName = "September";
               break;
            case "11":
               monthName = "October";
               break;
            case "12":
               monthName = "November";
               break;        
       }
       
       Mon.setText(monthName + " " + year);
       
       try{
            float hours = 0;
            float Basic= 0;
            float HouseRent= 0;
            float Conveyance= 0;
            float CLA= 0;
            float Medical= 0;
            float Special= 0;
            float OverTime= 0;
            float Bonous= 0;
            float PFCounter= 0;
            float OtherDED2= 0;
            float EOBIDED= 0;
            float Other= 0;
            float TotalEarn= 0;
            float TotalDeduc= 0;
            float NetSalary= 0;
            
            Connection c = DBCON.con();
            Statement st = c.createStatement();
            String sql = "SELECT * FROM workinghours WHERE EmpCode ='" + EmpCode.getSelectedItem()+ "' AND Year = '" + year+ "' AND Month ='" + monthName +"' ";
            ResultSet rs = st.executeQuery(sql);
            
            while (rs.next()) {
                hours = Float.parseFloat(rs.getString("Hours"));
                             
            }
            
            String sql1 = "SELECT * FROM earning_info WHERE EmpCode ='" + EmpCode.getSelectedItem()+"' ";
            ResultSet rs1 = st.executeQuery(sql1);
            
            while (rs1.next()) {
                Basic = Float.parseFloat(rs1.getString("Basic"));
                HouseRent = Float.parseFloat(rs1.getString("HouseRent"));
                Conveyance = Float.parseFloat(rs1.getString("Conveyance"));
                CLA = Float.parseFloat(rs1.getString("CLA"));
                Medical = Float.parseFloat(rs1.getString("Medical"));
                Special = Float.parseFloat(rs1.getString("Special"));
                OverTime = Float.parseFloat(rs1.getString("OverTime"));
                Bonous = Float.parseFloat(rs1.getString("Bonous"));
                PFCounter = Float.parseFloat(rs1.getString("PFCounter"));
                OtherDED2 = Float.parseFloat(rs1.getString("OtherDED2"));
                EOBIDED = Float.parseFloat(rs1.getString("EOBIDED"));
                Other = Float.parseFloat(rs1.getString("Other"));                             
            }
            
            if(hours>160){
                
                float newOverTime = (hours - 160)*OverTime;
                float newBonus  = Basic*Bonous/100;
                float newPFCounter = Basic*PFCounter/100;
                float newEOBIDED = Basic * EOBIDED/100;
                
                TotalEarn = Basic + HouseRent + Conveyance + CLA + Medical + Special + newOverTime + newBonus;
                TotalDeduc = newPFCounter + OtherDED2 + newEOBIDED + Other;
                NetSalary = TotalEarn - TotalDeduc;
                
                this.Bonous.setText(newBonus+"");
                this.Basic.setText(Basic+"");
                this.HouseRent.setText(HouseRent+"");
                this.Conveyance.setText(Conveyance+"");
                this.CLA.setText(CLA+"");
                this.Medical.setText(Medical+"");
                this.Special.setText(Special+"");
                this.OverTime.setText(newOverTime+"");
                this.PFCounter.setText(newPFCounter+"");
                this.OtherDED2.setText(OtherDED2+"");
                this.EOBIDED.setText(newEOBIDED+"");
                this.Other.setText(Other+"");
                this.TotalEarnings.setText(TotalEarn+"");
                this.TotalDeduction.setText(TotalDeduc+"");
                this.NetSalary.setText(NetSalary+"");
                
            }else{
                float newOverTime = 0;
                float newBonus  = Basic*Bonous/100;
                float newPFCounter = Basic*PFCounter/100;
                float newEOBIDED = Basic * EOBIDED/100;
                
                TotalEarn = Basic + HouseRent + Conveyance + CLA + Medical + Special + newOverTime + newBonus;
                TotalDeduc = newPFCounter + OtherDED2 + newEOBIDED + Other;
                NetSalary = TotalEarn - TotalDeduc;
                
                this.Bonous.setText(newBonus+"");
                this.Basic.setText(Basic+"");
                this.HouseRent.setText(HouseRent+"");
                this.Conveyance.setText(Conveyance+"");
                this.CLA.setText(CLA+"");
                this.Medical.setText(Medical+"");
                this.Special.setText(Special+"");
                this.OverTime.setText(newOverTime+"");
                this.PFCounter.setText(newPFCounter+"");
                this.OtherDED2.setText(OtherDED2+"");
                this.EOBIDED.setText(newEOBIDED+"");
                this.Other.setText(Other+"");
                this.TotalEarnings.setText(TotalEarn+"");
                this.TotalDeduction.setText(TotalDeduc+"");
                this.NetSalary.setText(NetSalary+"");
            }
            
            
           
       }catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        NIC = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        AppCode = new javax.swing.JLabel();
        EOBIDED = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Basic = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        OverTime = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        Medical = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        PFCounter = new javax.swing.JLabel();
        TotalEarnings = new javax.swing.JLabel();
        Mon = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        NetSalary = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        Special = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        CLA = new javax.swing.JLabel();
        TypeOfEmployee = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        OtherDED2 = new javax.swing.JLabel();
        NameInitial = new javax.swing.JLabel();
        Conveyance = new javax.swing.JLabel();
        TotalDeduction = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        Designation = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        Other = new javax.swing.JLabel();
        Bonous = new javax.swing.JLabel();
        HouseRent = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        Grade = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        EmpCode = new javax.swing.JComboBox<>();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NIC.setBackground(new java.awt.Color(255, 255, 255));
        NIC.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NIC.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NIC.setOpaque(true);
        getContentPane().add(NIC, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 170, 170, 20));

        jLabel31.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 51, 51));
        jLabel31.setText("Earnings");
        getContentPane().add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 300, 138, -1));

        AppCode.setBackground(new java.awt.Color(255, 255, 255));
        AppCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        AppCode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        AppCode.setOpaque(true);
        getContentPane().add(AppCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 170, 20));

        EOBIDED.setBackground(new java.awt.Color(255, 255, 255));
        EOBIDED.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        EOBIDED.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(EOBIDED, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 370, 170, 20));

        jLabel36.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(32, 194, 170));
        jLabel36.setText("Other");
        getContentPane().add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 390, 138, -1));

        jLabel23.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(32, 194, 170));
        jLabel23.setText("Bonous");
        getContentPane().add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 470, 138, -1));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(32, 194, 170));
        jLabel3.setText("Applicant Code");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 138, -1));

        Basic.setBackground(new java.awt.Color(255, 255, 255));
        Basic.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Basic.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Basic, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 330, 170, 20));

        jLabel34.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(32, 194, 170));
        jLabel34.setText("Other DED 2");
        getContentPane().add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 350, 138, -1));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(32, 194, 170));
        jLabel4.setText("Name");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 138, -1));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(32, 194, 170));
        jLabel5.setText("NIC");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 138, -1));

        OverTime.setBackground(new java.awt.Color(255, 255, 255));
        OverTime.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        OverTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(OverTime, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 450, 170, 20));

        jLabel17.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(32, 194, 170));
        jLabel17.setText("Designation");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 80, 138, -1));

        jLabel22.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(32, 194, 170));
        jLabel22.setText("Over Time");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 450, 138, -1));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("* For the previous pay Slips use Report section");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 560, 310, 20));

        jLabel14.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(32, 194, 170));
        jLabel14.setText("Medical");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 410, 138, -1));

        jLabel30.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(32, 194, 170));
        jLabel30.setText("Basic");
        getContentPane().add(jLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 330, 138, -1));

        Medical.setBackground(new java.awt.Color(255, 255, 255));
        Medical.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Medical.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Medical, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 410, 170, 20));

        jLabel18.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(32, 194, 170));
        jLabel18.setText("Grade");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 110, 138, -1));

        jLabel13.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(32, 194, 170));
        jLabel13.setText("C.L.A.");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 390, 138, -1));

        jLabel15.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(32, 194, 170));
        jLabel15.setText("Special");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 430, 138, -1));

        PFCounter.setBackground(new java.awt.Color(255, 255, 255));
        PFCounter.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        PFCounter.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(PFCounter, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 330, 170, 20));

        TotalEarnings.setBackground(new java.awt.Color(255, 255, 255));
        TotalEarnings.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        TotalEarnings.setForeground(new java.awt.Color(255, 51, 51));
        TotalEarnings.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(TotalEarnings, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 500, 170, 20));

        Mon.setFont(new java.awt.Font("Century Gothic", 0, 36)); // NOI18N
        Mon.setForeground(new java.awt.Color(255, 51, 51));
        Mon.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        getContentPane().add(Mon, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 220, 330, 50));

        jLabel12.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(32, 194, 170));
        jLabel12.setText("Conveyance");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 370, 138, -1));

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(32, 194, 170));
        jLabel2.setText("Employee Code");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 138, -1));

        jLabel11.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(32, 194, 170));
        jLabel11.setText("House Rent");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 350, 138, -1));

        jLabel29.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(32, 194, 170));
        jLabel29.setText("PF-Counter");
        getContentPane().add(jLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 330, 138, -1));

        jLabel35.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(32, 194, 170));
        jLabel35.setText("EOBI-DED");
        getContentPane().add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 370, 138, -1));

        NetSalary.setBackground(new java.awt.Color(255, 255, 255));
        NetSalary.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        NetSalary.setForeground(new java.awt.Color(255, 51, 51));
        NetSalary.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(NetSalary, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 530, 170, 20));

        jLabel16.setBackground(new java.awt.Color(32, 194, 170));
        jLabel16.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Print");
        jLabel16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel16.setOpaque(true);
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel16MouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel16MouseEntered(evt);
            }
        });
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 590, 100, 30));

        Special.setBackground(new java.awt.Color(255, 255, 255));
        Special.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Special.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Special, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 430, 170, 20));

        jLabel32.setFont(new java.awt.Font("Century Gothic", 0, 36)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(255, 51, 51));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel32.setText("Pay Slip for the Period of");
        getContentPane().add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 220, 430, 50));

        CLA.setBackground(new java.awt.Color(255, 255, 255));
        CLA.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        CLA.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(CLA, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 390, 170, 20));

        TypeOfEmployee.setBackground(new java.awt.Color(255, 255, 255));
        TypeOfEmployee.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        TypeOfEmployee.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        TypeOfEmployee.setOpaque(true);
        getContentPane().add(TypeOfEmployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 140, 170, 20));

        jLabel28.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 51, 51));
        jLabel28.setText("Deductions");
        getContentPane().add(jLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 300, 138, -1));

        OtherDED2.setBackground(new java.awt.Color(255, 255, 255));
        OtherDED2.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        OtherDED2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(OtherDED2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 350, 170, 20));

        NameInitial.setBackground(new java.awt.Color(255, 255, 255));
        NameInitial.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NameInitial.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NameInitial.setOpaque(true);
        getContentPane().add(NameInitial, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, 170, 20));

        Conveyance.setBackground(new java.awt.Color(255, 255, 255));
        Conveyance.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Conveyance.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Conveyance, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 370, 170, 20));

        TotalDeduction.setBackground(new java.awt.Color(255, 255, 255));
        TotalDeduction.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        TotalDeduction.setForeground(new java.awt.Color(255, 51, 51));
        TotalDeduction.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(TotalDeduction, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 500, 170, 20));

        jLabel19.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(32, 194, 170));
        jLabel19.setText("Type of Employee");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 140, 138, -1));

        Designation.setBackground(new java.awt.Color(255, 255, 255));
        Designation.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Designation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Designation.setOpaque(true);
        getContentPane().add(Designation, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 80, 170, 20));

        jLabel26.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 51, 51));
        jLabel26.setText("Total Deductions");
        getContentPane().add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 500, 138, -1));

        Other.setBackground(new java.awt.Color(255, 255, 255));
        Other.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Other.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Other, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 390, 170, 20));

        Bonous.setBackground(new java.awt.Color(255, 255, 255));
        Bonous.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Bonous.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(Bonous, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 470, 170, 20));

        HouseRent.setBackground(new java.awt.Color(255, 255, 255));
        HouseRent.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        HouseRent.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(HouseRent, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 350, 170, 20));

        Date.setBackground(new java.awt.Color(255, 255, 255));
        Date.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Date.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Date.setOpaque(true);
        getContentPane().add(Date, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 170, 170, 20));

        jLabel24.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 51, 51));
        jLabel24.setText("Total Earnings");
        getContentPane().add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 500, 138, -1));

        jLabel21.setFont(new java.awt.Font("Century Gothic", 0, 13)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 51, 51));
        jLabel21.setText("Net Salary");
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 530, 138, -1));

        Grade.setBackground(new java.awt.Color(255, 255, 255));
        Grade.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Grade.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Grade.setOpaque(true);
        getContentPane().add(Grade, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 110, 170, 20));

        jLabel20.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(32, 194, 170));
        jLabel20.setText("Date");
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 170, 138, -1));

        jLabel40.setBackground(new java.awt.Color(32, 194, 170));
        jLabel40.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("Back");
        jLabel40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel40.setOpaque(true);
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel40MouseExited(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel40MouseEntered(evt);
            }
        });
        getContentPane().add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(650, 590, 100, 30));

        EmpCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        EmpCode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                EmpCodeMouseClicked(evt);
            }
        });
        EmpCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EmpCodeActionPerformed(evt);
            }
        });
        getContentPane().add(EmpCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 170, -1));

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hrms/Payroll2.jpg"))); // NOI18N
        getContentPane().add(background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 680));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel16MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseExited
        jLabel16.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel16MouseExited

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel16MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseEntered
        jLabel16.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel16MouseEntered

    private void jLabel40MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseExited
        jLabel40.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel40MouseExited

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        // TODO add your handling code here:
        new AdminHome2().setVisible(true);
        close();
    }//GEN-LAST:event_jLabel40MouseClicked

    private void jLabel40MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseEntered
        jLabel40.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel40MouseEntered

    private void EmpCodeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EmpCodeMouseClicked
        
    }//GEN-LAST:event_EmpCodeMouseClicked

    private void EmpCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EmpCodeActionPerformed
        String AppCode = "";
        String NameInitial = "";
        String Designation = "";
        String Grade = "";
        String TypeOfEmployee = "";                               
        String NIC = "";                
        
        try {
            Connection c = DBCON.con();
            Statement st = c.createStatement();
            String sql = "SELECT * FROM personal_info WHERE EmpCode  ='" + EmpCode.getSelectedItem() + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                AppCode = rs.getString("AppCode");                
                NameInitial = rs.getString("NameInitial");
                Designation = rs.getString("Designation");
                Grade = rs.getString("Grade");
                TypeOfEmployee = rs.getString("TypeOfEmployee");               
                NIC = rs.getString("NIC");
                

                this.AppCode.setText(AppCode);
                this.NameInitial.setText(NameInitial);
                this.Designation.setText(Designation);
                this.Grade.setText(Grade);
                this.TypeOfEmployee.setText(TypeOfEmployee);                
                this.NIC.setText(NIC);                
            }
            calculteSalary();

        } catch (Exception e) {
            System.out.println(e);
            
        }
    }//GEN-LAST:event_EmpCodeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PayrollMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PayrollMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PayrollMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PayrollMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PayrollMGR().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AppCode;
    private javax.swing.JLabel Basic;
    private javax.swing.JLabel Bonous;
    private javax.swing.JLabel CLA;
    private javax.swing.JLabel Conveyance;
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Designation;
    private javax.swing.JLabel EOBIDED;
    private javax.swing.JComboBox<String> EmpCode;
    private javax.swing.JLabel Grade;
    private javax.swing.JLabel HouseRent;
    private javax.swing.JLabel Medical;
    private javax.swing.JLabel Mon;
    private javax.swing.JLabel NIC;
    private javax.swing.JLabel NameInitial;
    private javax.swing.JLabel NetSalary;
    private javax.swing.JLabel Other;
    private javax.swing.JLabel OtherDED2;
    private javax.swing.JLabel OverTime;
    private javax.swing.JLabel PFCounter;
    private javax.swing.JLabel Special;
    private javax.swing.JLabel TotalDeduction;
    private javax.swing.JLabel TotalEarnings;
    private javax.swing.JLabel TypeOfEmployee;
    private javax.swing.JLabel background;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
