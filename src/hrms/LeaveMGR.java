/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hrms;

import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Chamidu Chanaka
 */
public class LeaveMGR extends javax.swing.JFrame {

    /**
     * Creates new form LeaveMGR
     */
    public LeaveMGR(String user) {
        initComponents();
        String nic = " ";
        String newNic = " ";
        String Ecode = "";
        String AppCode = "";
        String name = "";
        String des = "";
        String grade = "";
        String type = "";

        Date d = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf1.format(d);
        RemainingLeaves.setText(date);

        try {

            Connection c = DBCON.con();
            Statement st = c.createStatement();
            String sql = "SELECT NIC FROM Logdata WHERE UserName ='" + user + "'";
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                nic = rs.getString("NIC");

            }

            String sql2 = "SELECT * FROM personal_info WHERE NIC ='" + nic + "'";
            ResultSet rs2 = st.executeQuery(sql2);

            while (rs2.next()) {
                newNic = rs2.getString("NIC");

                Ecode = rs2.getString("EmpCode");
                AppCode = rs2.getString("AppCode");
                name = rs2.getString("NameInitial");
                des = rs2.getString("Designation");
                grade = rs2.getString("Grade");
                type = rs2.getString("TypeOfEmployee");

                NIC.setText(newNic);
                this.EmpCode.setText(Ecode);
                this.AppCode.setText(AppCode);
                NameInitial.setText(name);
                Designation.setText(des);
                Grade.setText(grade);
                TypeOfEmployee.setText(type);
            }

            String sql3 = "SELECT * FROM leave_info";
            ResultSet rs3 = st.executeQuery(sql3);

            int i = 0;
            String LeaveIdT = "";
            String LeaveTypeT = "";
            String StartDateT = "";
            String NoOfDaysT = "";
            String LeavePeriodT = "";
            String DescriptionT = "";
            String ContactNoT = "";
            String StatusT = "";
            String EcodeT = "";

            while (rs3.next()) {
                EcodeT = rs3.getString("EmpCode");
                LeaveIdT = rs3.getString("LeaveId");
                LeaveTypeT = rs3.getString("LeaveType");
                StartDateT = rs3.getString("StartDate");
                NoOfDaysT = rs3.getString("NoOfDays");
                LeavePeriodT = rs3.getString("LeavePeriod");
                DescriptionT = rs3.getString("Description");
                ContactNoT = rs3.getString("ContactNo");
                StatusT = rs3.getString("Status");

                leaveTable.setValueAt(EcodeT, i, 0);
                leaveTable.setValueAt(LeaveIdT, i, 1);
                leaveTable.setValueAt(LeaveTypeT, i, 2);
                leaveTable.setValueAt(StartDateT, i, 3);
                leaveTable.setValueAt(NoOfDaysT, i, 4);
                leaveTable.setValueAt(LeavePeriodT, i, 5);
                leaveTable.setValueAt(DescriptionT, i, 6);
                leaveTable.setValueAt(ContactNoT, i, 7);
                leaveTable.setValueAt(StatusT, i, 8);
                i++;
            }

        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(null, e);
        }

        tableFirstRow();

    }

    public void tableFirstRow() {
        String LeaveIdT = "";
        String LeaveTypeT = "";
        String StartDateT = "";
        String NoOfDaysT = "";
        String LeavePeriodT = "";
        String DescriptionT = "";
        String ContactNoT = "";
        String StatusT = "";
        String EcodeT = "";

        EcodeT = leaveTable.getValueAt(0, 0).toString();
        LeaveIdT = leaveTable.getValueAt(0, 1).toString();
        LeaveTypeT = leaveTable.getValueAt(0, 2).toString();
        StartDateT = leaveTable.getValueAt(0, 3).toString();
        NoOfDaysT = leaveTable.getValueAt(0, 4).toString();
        LeavePeriodT = leaveTable.getValueAt(0, 5).toString();
        DescriptionT = leaveTable.getValueAt(0, 6).toString();
        ContactNoT = leaveTable.getValueAt(0, 7).toString();
        StatusT = leaveTable.getValueAt(0, 8).toString();

        EmpCode1.setText(EcodeT);
        LeaveId.setText(LeaveIdT);
        LeaveType.setText(LeaveTypeT);
        StartDate.setText(StartDateT);
        NoOfDays.setText(NoOfDaysT);
        LeavePeriod.setText(LeavePeriodT);
        Description.setText(DescriptionT);
        ContactNo.setText(ContactNoT);
        Status.setSelectedItem(StatusT);

    }

    public void close() {
        WindowEvent winClosingEvent = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
        Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(winClosingEvent);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel11 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        RemainingLeaves = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        leaveTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        StartDate = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        EmpCode = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        LeaveId = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        NoOfDays = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        ContactNo = new javax.swing.JTextArea();
        AppCode = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        NIC = new javax.swing.JLabel();
        TypeOfEmployee = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        LeaveType = new javax.swing.JTextField();
        LeavePeriod = new javax.swing.JTextField();
        Grade = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        Description = new javax.swing.JTextArea();
        Designation = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        NameInitial = new javax.swing.JLabel();
        EmpCode1 = new javax.swing.JTextField();
        Status = new javax.swing.JComboBox<>();
        jLabel40 = new javax.swing.JLabel();
        BackGround = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(32, 194, 170));
        jLabel11.setText("Leave Period");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 450, 110, 20));

        jLabel14.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(32, 194, 170));
        jLabel14.setText("Contact No, Address ");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 480, 130, 20));

        RemainingLeaves.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        RemainingLeaves.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        RemainingLeaves.setOpaque(true);
        getContentPane().add(RemainingLeaves, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 170, 170, 20));

        jLabel20.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(32, 194, 170));
        jLabel20.setText("Date");
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 170, 138, -1));

        jLabel12.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(32, 194, 170));
        jLabel12.setText("No of Days");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 420, 110, 20));

        leaveTable.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        leaveTable.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        leaveTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Employee Code", "Leave ID", "Leave Type", "Start Date", "No of days", "Leave Peiod", "Description", "Contact No/Address", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        leaveTable.setGridColor(new java.awt.Color(32, 194, 170));
        leaveTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                leaveTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(leaveTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 290, 420, 170));

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(32, 194, 170));
        jLabel1.setText("Employee Code");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 138, -1));

        StartDate.setEditable(false);
        StartDate.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(StartDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 390, 250, 20));

        jLabel2.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(32, 194, 170));
        jLabel2.setText("Applicant Code");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 110, 138, -1));

        EmpCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        EmpCode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        EmpCode.setOpaque(true);
        getContentPane().add(EmpCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 170, 20));

        jLabel13.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(32, 194, 170));
        jLabel13.setText("Description");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 480, 110, 20));

        LeaveId.setEditable(false);
        LeaveId.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(LeaveId, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 330, 250, 20));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(32, 194, 170));
        jLabel7.setText("Start Date");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 390, 110, 20));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(32, 194, 170));
        jLabel9.setText("(During Leave Period)");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 500, 130, 20));

        NoOfDays.setEditable(false);
        NoOfDays.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(NoOfDays, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 420, 250, 20));

        ContactNo.setEditable(false);
        ContactNo.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        ContactNo.setColumns(20);
        ContactNo.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        ContactNo.setRows(5);
        jScrollPane3.setViewportView(ContactNo);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 480, 280, 70));

        AppCode.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        AppCode.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        AppCode.setOpaque(true);
        getContentPane().add(AppCode, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 110, 170, 20));

        jLabel6.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(32, 194, 170));
        jLabel6.setText("Leave Type");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 360, 110, 20));

        NIC.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NIC.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NIC.setOpaque(true);
        getContentPane().add(NIC, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 170, 170, 20));

        TypeOfEmployee.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        TypeOfEmployee.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        TypeOfEmployee.setOpaque(true);
        getContentPane().add(TypeOfEmployee, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 140, 170, 20));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(32, 194, 170));
        jLabel5.setText("Emp. Code");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 300, 110, 20));

        jLabel18.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(32, 194, 170));
        jLabel18.setText("Grade");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 110, 138, -1));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(32, 194, 170));
        jLabel4.setText("NIC");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 138, -1));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(32, 194, 170));
        jLabel3.setText("Name");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 138, -1));

        jLabel19.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(32, 194, 170));
        jLabel19.setText("Type of Employee");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 140, 138, -1));

        jLabel17.setFont(new java.awt.Font("Century Gothic", 1, 11)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(32, 194, 170));
        jLabel17.setText("Designation");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 80, 138, -1));

        LeaveType.setEditable(false);
        LeaveType.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(LeaveType, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 360, 250, 20));

        LeavePeriod.setEditable(false);
        LeavePeriod.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(LeavePeriod, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 450, 250, 20));

        Grade.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Grade.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Grade.setOpaque(true);
        getContentPane().add(Grade, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 110, 170, 20));

        jLabel10.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(32, 194, 170));
        jLabel10.setText("Status");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 560, 110, 20));

        jLabel39.setBackground(new java.awt.Color(32, 194, 170));
        jLabel39.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel39.setForeground(new java.awt.Color(255, 255, 255));
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel39.setText("Back");
        jLabel39.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel39.setOpaque(true);
        jLabel39.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel39MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel39MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel39MouseExited(evt);
            }
        });
        getContentPane().add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 580, 100, 30));

        Description.setEditable(false);
        Description.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        Description.setColumns(20);
        Description.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        Description.setRows(5);
        jScrollPane2.setViewportView(Description);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 480, 250, 70));

        Designation.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Designation.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Designation.setOpaque(true);
        getContentPane().add(Designation, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 80, 170, 20));

        jLabel25.setFont(new java.awt.Font("Century Gothic", 0, 36)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 51, 51));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("Leave Application Details");
        getContentPane().add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 210, 520, -1));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(32, 194, 170));
        jLabel8.setText("Leave ID");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 330, 110, 20));

        NameInitial.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        NameInitial.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        NameInitial.setOpaque(true);
        getContentPane().add(NameInitial, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 140, 170, 20));

        EmpCode1.setEditable(false);
        EmpCode1.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        getContentPane().add(EmpCode1, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 300, 250, 20));

        Status.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        Status.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pending", "Accepted", "Rejected" }));
        getContentPane().add(Status, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 560, 280, -1));

        jLabel40.setBackground(new java.awt.Color(32, 194, 170));
        jLabel40.setFont(new java.awt.Font("Century Gothic", 0, 15)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("Save");
        jLabel40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel40.setOpaque(true);
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel40MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel40MouseExited(evt);
            }
        });
        getContentPane().add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 580, 100, 30));

        BackGround.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hrms/Leave.jpg"))); // NOI18N
        getContentPane().add(BackGround, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 950, 680));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void leaveTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_leaveTableMouseClicked
        String LeaveIdT = "";
        String LeaveTypeT = "";
        String StartDateT = "";
        String NoOfDaysT = "";
        String LeavePeriodT = "";
        String DescriptionT = "";
        String ContactNoT = "";
        String StatusT = "";
        String EcodeT = "";

        int row = leaveTable.getSelectedRow();

        EcodeT = leaveTable.getValueAt(row, 0).toString();
        LeaveIdT = leaveTable.getValueAt(row, 1).toString();
        LeaveTypeT = leaveTable.getValueAt(row, 2).toString();
        StartDateT = leaveTable.getValueAt(row, 3).toString();
        NoOfDaysT = leaveTable.getValueAt(row, 4).toString();
        LeavePeriodT = leaveTable.getValueAt(row, 5).toString();
        DescriptionT = leaveTable.getValueAt(row, 6).toString();
        ContactNoT = leaveTable.getValueAt(row, 7).toString();
        StatusT = leaveTable.getValueAt(row, 8).toString();

        EmpCode1.setText(EcodeT);
        LeaveId.setText(LeaveIdT);
        LeaveType.setText(LeaveTypeT);
        StartDate.setText(StartDateT);
        NoOfDays.setText(NoOfDaysT);
        LeavePeriod.setText(LeavePeriodT);
        Description.setText(DescriptionT);
        ContactNo.setText(ContactNoT);
        Status.setSelectedItem(StatusT);
    }//GEN-LAST:event_leaveTableMouseClicked

    private void jLabel39MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseExited
        jLabel39.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel39MouseExited

    private void jLabel39MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseClicked
        new AdminHome2().setVisible(true);
        close();
    }//GEN-LAST:event_jLabel39MouseClicked

    private void jLabel39MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel39MouseEntered
        jLabel39.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel39MouseEntered

    private void jLabel40MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseExited
        jLabel40.setBackground(new java.awt.Color(32, 194, 170));
    }//GEN-LAST:event_jLabel40MouseExited

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        try {

            Connection c = DBCON.con();
            Statement s = c.createStatement();
            s.executeUpdate("update leave_info set Status ='" + Status.getSelectedItem() + "' where LeaveId ='" + LeaveId.getText() + "'");
            JOptionPane.showMessageDialog(null, "Upadate Successfully");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jLabel40MouseClicked

    private void jLabel40MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseEntered
        jLabel40.setBackground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel40MouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LeaveMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LeaveMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LeaveMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LeaveMGR.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LeaveMGR(Login.UserName.getText()).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AppCode;
    private javax.swing.JLabel BackGround;
    private javax.swing.JTextArea ContactNo;
    private javax.swing.JTextArea Description;
    private javax.swing.JLabel Designation;
    private javax.swing.JLabel EmpCode;
    private javax.swing.JTextField EmpCode1;
    private javax.swing.JLabel Grade;
    private javax.swing.JTextField LeaveId;
    private javax.swing.JTextField LeavePeriod;
    private javax.swing.JTextField LeaveType;
    private javax.swing.JLabel NIC;
    private javax.swing.JLabel NameInitial;
    private javax.swing.JTextField NoOfDays;
    private javax.swing.JLabel RemainingLeaves;
    private javax.swing.JTextField StartDate;
    private javax.swing.JComboBox<String> Status;
    private javax.swing.JLabel TypeOfEmployee;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable leaveTable;
    // End of variables declaration//GEN-END:variables
}
